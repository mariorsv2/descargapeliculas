/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Vector;

/**Class Servs donde esta el hilo para conectar otros clientes
 * @since ServidorDescarga
 * @author Jose Suarez
 * @author Mario Salazar
 */
public class Servs extends Thread  {
    Socket scli=null;//Socket para la comunicacion
    Socket scli2=null;//Socket para la comunicacion
    Socket sserv=null;//Socket para la comunicacion
    Socket cli=null;//Socket para la comunicacion
    int sum=0,subtotal=0;//Integer para llevar conteo del buffer
    
    DataInputStream entrada=null, in=null;//Se crea objetos DataInput para ver lo que manda el cliente  
    DataOutputStream salida2=null, out=null;//Se crea objetos DataOutput para ver lo que se le manda al cliente
    public static ObjectInputStream entrante = null;//Se crea un objeto para saber que entra al servidor
    public static ObjectOutputStream  saliente = null;//Se crea un objeto para saber que sale del servidor
    public static Vector<Socket> clientes=new Vector();//Se crea un vector para saber que clientes hay conectado al servidor
    
        final String filename = "./src/Peliculas/Anaconda.mp4";//Las rutas donde se encuentran las peliculas
        final String filename2 = "./src/Peliculas/Booty.mp4";//Las rutas donde se encuentran las peliculas        
     public static int vecesDescargada=1,vecesDescargada1=1,vecesDescargada2=1,vecesDescargada3=1;//Variables int para llevar el conteo de cuantas veces han sido descargadas
     ServidorDescarga serv;//Se crea un servidor socket donde se inicializa para aceptar las peticiones

 /**Constructor Servs donde esta el hilo para manejarse los clientes
  * @param scliente donde viene el socket del cliente
  * @param serv donde viene el socket del servidor
 * @since ServidorDescarga
 * @author Jose Suarez
 * @author Mario Salazar
 */

     
      public Servs(Socket scliente,ServidorDescarga serv) throws IOException{
        scli=scliente; //Se guarda el cliente
        this.serv=serv;//Se instancia el servidor
        clientes.add(scli); //Se guarda el socket del cliente en el vector
        serv.mostrar("cliente agregado: "+ scli);	//Se muestra el cliente que se agrego con su ip y su puerto		
     }
  /**Metodo para correr el servidor
 * @since ServidorDescarga
 * @author Jose Suarez
 * @author Mario Salazar
 */    
      public void run(){
    	
    	
    	try
    	{//inicio de Try Catch
          entrada=new DataInputStream(scli.getInputStream());//Se crea un objeto de tipo DataInput para saber la data que manda el servidor
          salida2=new DataOutputStream(scli.getOutputStream());//Se crea un objeto de tipo DataOutpu para saber la data que se manda al cliente  
          salida2.writeUTF(".:: Bienvenido  :");//Se le envia al cliente 
          salida2.writeUTF(".::Peliculas Disponibles :");//Se le envia al cliente 
          salida2.writeUTF("Booty");//Se le envia al cliente 
          salida2.writeUTF("Anaconda");//Se le envia al cliente 
          salida2.writeUTF("Boota");//Se le envia al cliente 
          salida2.writeUTF("Serpiente");//Se le envia al cliente 
          
        
    	}
    	catch (IOException e) {  e.printStackTrace();     }//Se captura la exception
    	
                
    	while(true)
    	{//inicio del while true
          try
          {//Inicio del try catch
              String mensaje="";//Se inicializa un mensaje 
              mensaje=entrada.readUTF();//Se lee lo que manda el cliente
              if (mensaje.equals("Booty")){//Si el mensaje es igual a Booty
                 String Pelicula="Booty Cliente: "+scli.getInetAddress().toString();//Se guarda en Pelicula el cliente mas la pelicula
                 ServidorDescarga.EscribirPeliculasDescargando(Pelicula);//Se invoca el metodo para escribir en el archivo
                 cli=new Socket(scli.getInetAddress(),8089);//Se crea un socket para enviar el archivo al cliente
                 in=new DataInputStream(cli.getInputStream());//Se crea un Datainput para la entrada que envia el cliente
                 out=new DataOutputStream(cli.getOutputStream());//Se crea un Dataoutput para la salida que se le envia al cliente
                 PrintStream envio=new PrintStream(cli.getOutputStream());//Se crea un PrintStream para el envio del video
                 File extra=new File(filename2);//Se abre el video
                 int tamaño=(int) extra.length();//Se guarda el tamaño del archivo
                 out.writeUTF(Integer.toString(tamaño));//Se le manda al cliente el tamaño del archivo
                 out.writeUTF("Booty.mp4");//Se le manda al cliente el nombre del archivo
                 FileInputStream origen=new FileInputStream(filename2);//Se crea un PrintStream desde el origen del archivo
                 byte[] buffer = new byte[1024]; //Se crea un buffer de 1024 bytes
                 int len;//Se inicializa para saber cuanto lleva de buffer
                 while((len=origen.read(buffer))>0){//Se inicia el while mientras el buffer sea mayor a 0
                     envio.write(buffer,0,len);//Se envia lo que lleva en buffer al cliente
                     envio.flush();//Se envia lo que lleva
                    } 
                       ServidorDescarga.EscribirPeliculasDescargada("Booty", vecesDescargada2++);//Se Guarda el nombre de la pelicula y cuantas veces se han descargado
                      
                       envio.close();//Se cierra el PrintStream
                       
              }
              if (mensaje.equals("Anaconda")){
                  String Pelicula="Anaconda Cliente: "+scli.getInetAddress().toString();//Se guarda en Pelicula el cliente mas la pelicula
                 ServidorDescarga.EscribirPeliculasDescargando(Pelicula);//Se invoca el metodo para escribir en el archivo
                 cli=new Socket(scli.getInetAddress(),8089);//Se crea un socket para enviar el archivo al cliente
                 in=new DataInputStream(cli.getInputStream());//Se crea un Datainput para la entrada que envia el cliente
                 out=new DataOutputStream(cli.getOutputStream());//Se crea un Dataoutput para la salida que se le envia al cliente
                 PrintStream envio=new PrintStream(cli.getOutputStream());//Se crea un PrintStream para el envio del video
                 File extra=new File(filename);//Se abre el video
                 int tamaño=(int) extra.length();//Se guarda el tamaño del archivo
                 out.writeUTF(Integer.toString(tamaño));//Se le manda al cliente el tamaño del archivo
                 out.writeUTF("Anaconda.mp4");//Se le manda al cliente el nombre del archivo
                 FileInputStream origen=new FileInputStream(filename);//Se crea un PrintStream desde el origen del archivo
                 byte[] buffer = new byte[1024]; //Se crea un buffer de 1024 bytes
                 int len;//Se inicializa para saber cuanto lleva de buffer
                 while((len=origen.read(buffer))>0){//Se inicia el while mientras el buffer sea mayor a 0
                     envio.write(buffer,0,len);//Se envia lo que lleva en buffer al cliente
                     envio.flush();//Se envia lo que lleva
                    } 
                       ServidorDescarga.EscribirPeliculasDescargada("Anaconda", vecesDescargada2++);//Se Guarda el nombre de la pelicula y cuantas veces se han descargado
                      
                       envio.close();//Se cierra el PrintStream
              }
              if (mensaje.equals("Boota")){
                String Pelicula="Boota Cliente: "+scli.getInetAddress().toString();//Se guarda en Pelicula el cliente mas la pelicula
                 ServidorDescarga.EscribirPeliculasDescargando(Pelicula);//Se invoca el metodo para escribir en el archivo
                 cli=new Socket(scli.getInetAddress(),8089);//Se crea un socket para enviar el archivo al cliente
                 in=new DataInputStream(cli.getInputStream());//Se crea un Datainput para la entrada que envia el cliente
                 out=new DataOutputStream(cli.getOutputStream());//Se crea un Dataoutput para la salida que se le envia al cliente
                 PrintStream envio=new PrintStream(cli.getOutputStream());//Se crea un PrintStream para el envio del video
                 File extra=new File(filename2);//Se abre el video
                 int tamaño=(int) extra.length();//Se guarda el tamaño del archivo
                 out.writeUTF(Integer.toString(tamaño));//Se le manda al cliente el tamaño del archivo
                 out.writeUTF("Booty.mp4");//Se le manda al cliente el nombre del archivo
                 FileInputStream origen=new FileInputStream(filename2);//Se crea un PrintStream desde el origen del archivo
                 byte[] buffer = new byte[1024]; //Se crea un buffer de 1024 bytes
                 int len;//Se inicializa para saber cuanto lleva de buffer
                 while((len=origen.read(buffer))>0){//Se inicia el while mientras el buffer sea mayor a 0
                     envio.write(buffer,0,len);//Se envia lo que lleva en buffer al cliente
                     envio.flush();//Se envia lo que lleva
                    } 
                       ServidorDescarga.EscribirPeliculasDescargada("Boota", vecesDescargada2++);//Se Guarda el nombre de la pelicula y cuantas veces se han descargado
                      
                       envio.close();//Se cierra el PrintStream
              }
              if (mensaje.equals("Serpiente")){
                 String Pelicula="Serpiente Cliente: "+scli.getInetAddress().toString();//Se guarda en Pelicula el cliente mas la pelicula
                 ServidorDescarga.EscribirPeliculasDescargando(Pelicula);//Se invoca el metodo para escribir en el archivo
                 cli=new Socket(scli.getInetAddress(),8089);//Se crea un socket para enviar el archivo al cliente
                 in=new DataInputStream(cli.getInputStream());//Se crea un Datainput para la entrada que envia el cliente
                 out=new DataOutputStream(cli.getOutputStream());//Se crea un Dataoutput para la salida que se le envia al cliente
                 PrintStream envio=new PrintStream(cli.getOutputStream());//Se crea un PrintStream para el envio del video
                 File extra=new File(filename);//Se abre el video
                 int tamaño=(int) extra.length();//Se guarda el tamaño del archivo
                 out.writeUTF(Integer.toString(tamaño));//Se le manda al cliente el tamaño del archivo
                 out.writeUTF("Anaconda.mp4");//Se le manda al cliente el nombre del archivo
                 FileInputStream origen=new FileInputStream(filename);//Se crea un PrintStream desde el origen del archivo
                 byte[] buffer = new byte[1024]; //Se crea un buffer de 1024 bytes
                 int len;//Se inicializa para saber cuanto lleva de buffer
                 while((len=origen.read(buffer))>0){//Se inicia el while mientras el buffer sea mayor a 0
                     envio.write(buffer,0,len);//Se envia lo que lleva en buffer al cliente
                     envio.flush();//Se envia lo que lleva
                    } 
                       ServidorDescarga.EscribirPeliculasDescargada("Serpiente", vecesDescargada2++);//Se Guarda el nombre de la pelicula y cuantas veces se han descargado
                      
                       envio.close();//Se cierra el PrintStream
                        
              }
              if (mensaje.equals("Salir")){
                  salida2.writeUTF("FIN");
              }
             
          }
          catch (IOException e) {System.out.println("El cliente termino la conexion");break;}
    	}
    	serv.mostrar("Se removio un usuario");
    	
    	try
    	{
          serv.mostrar("Se desconecto un usuario");
         
          
          scli.close();
    	}	
        catch(Exception et)
        {serv.mostrar("no se puede cerrar el socket");}   
     } 
}
