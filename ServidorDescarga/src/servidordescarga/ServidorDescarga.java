/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;

/**Clase donde se inicializa el servidor descarga
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
       public class ServidorDescarga {
 
        String mensaje="";//Mensaje que envia el servidor
        static String peliculas="";//Peliculas que estan en el servidor
        
/**Metodo donde se muestra el mensaje en consola
 * @param msg mensaje que se muestra en consola 
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */        
      public static void mostrar(String msg){
       System.out.println(msg);//se imprime el mensaje en consola
   }
      
/**Metodo donde se modifica el archivo PeliculasDescargada.txt
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */        
      public static void Modificartxt(){
        try {//Inicia el try Catch
			
			BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/PeliculasDescargando.txt"));//Se crea un objeto BufferedReader donde ira el Archivo de .txt a leer
			String line = "";//Se inicializa para buscar si esta la linea a modificar 
			while((line = bufferedReader.readLine())!=null){//Se busca si la linea existe
				if(line.indexOf("Esto es linea ")!= -1){//Se pregunta si la linea es la que se busca
                                    line.replaceFirst(line, line);//Se reemplaza la linea
					System.out.println("Encontrado line: "+line);//Se imprime la linea nueva
				}
			}
		} catch (FileNotFoundException e) {e.printStackTrace();//Se captura la exception 
		} catch (IOException e) {e.printStackTrace();//Se captura la exception
		}
    }
      
 /**Metodo donde se modifica el archivo PeliculasDescargada.txt
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */          
      public static void Modificartxt2(){
        try {//Inicia el Try Catch
			BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/PeliculasDescargada.txt"));//Se crea un objeto BufferedReader donde ira el Archivo de .txt a leer
			String line = "";//Se inicializa para buscar si esta la linea a modificar
			while((line = bufferedReader.readLine())!=null){//Se busca si la linea existe
				if(line.indexOf("Esto es linea ")!= -1){//Se pregunta si la linea es la que se busca
                                    line.replaceFirst(line, line);//Se reemplaza la linea
					System.out.println("Encontrado line: "+line);//Se imprime la linea nueva
				}
			}
		} catch (FileNotFoundException e) {e.printStackTrace();//Se captura la exception
		} catch (IOException e) {e.printStackTrace();//Se captura la exception
		}
    }
      
 /**Metodo donde se escribe el archivo PeliculasDescargando.txt
 * @param Pelicula escribe la pelicula y desde donde se estan descargando
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
      
      public static void EscribirPeliculasDescargando(String Pelicula){
         try {//Inicia el Try Catch
			FileWriter escritura=new FileWriter("./src/PeliculasDescargando.txt");//Se crea un objeto FileWriter donde ira el Archivo de .txt a escribir
			
		        escritura.write("Pelicula Descargando: "+Pelicula+"\n");//Se escribe en el archivo las peliculas descargando
			escritura.close();//Se cierra el FileWriter
				
			

 
		} catch (IOException e) {//Se captura la exception
			// TODO Auto-generated catch block
			e.printStackTrace();//Se imprime la exception
		}
    }
      
/**Metodo donde se escribe el archivo PeliculasDescargada.txt
 * @param Pelicula escribe la pelicula
 * @param VecesDescargada escribe cuantas veces fue descargada la pelicula
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
      public static void EscribirPeliculasDescargada(String Pelicula,int VecesDescargada){
         try {//Inicio de Try Catch
			//Escritura
			FileWriter archivo = new FileWriter("./src/PeliculasDescargada.txt");//Se crea un objeto FileWriter donde ira el Archivo de .txt a escribir
			archivo.write("Pelicula Descargada: "+Pelicula+" "+VecesDescargada+"\n");//Se crea lo que se va escribir en el archivo
			archivo.close();
 
		} catch (IOException e) {//Se captura la exception
			// TODO Auto-generated catch block
			e.printStackTrace();//Se imprime la exception
		}
    }
      
/**Metodo donde se lee el archivo PeliculasDescargando.txt
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
      
      public static void Readtxt(){
            try {//Inicio de Try Catch
			BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/PeliculasDescargando.txt"));//Se crea un objeto BufferedReader donde ira el Archivo de .txt a leer
			String line = "";//Se inicializa para buscar en el archivo
			while((line = bufferedReader.readLine())!=null){//Se busca en el archivo
				if(line.indexOf("Pelicula Descargando: ")!= -1){//Si se consigue el archivo se imprime
					System.out.println(line);//Se imprime la linea
				}
			}
		} catch (FileNotFoundException e) {e.printStackTrace();//Captura la exception
		} catch (IOException e) {e.printStackTrace();//Captura la exception
		}
       }
      
/**Metodo donde se lee el archivo PeliculasDescargando.txt
 * @return String si consigue la pelicula
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
      public static String Readtxt2(){
            try {//Inicio del Try Catch
			BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/PeliculasDescargando.txt"));//Se crea un objeto BufferedReader donde ira el Archivo de .txt a leer
			String line = "";//Se inicializa para buscar en el archivo
			while((line = bufferedReader.readLine())!=null){//Se busca en el archivo
				if(line.indexOf("Pelicula Descargando: ")!= -1){//Si se consigue el archivo se imprime
				   peliculas=line+"/n";//Se guarda en Peliculas que esten
				}
			}
                        return peliculas;//Retorna Peliculas
		} catch (FileNotFoundException e) {e.printStackTrace();//Captura la exception
		} catch (IOException e) {e.printStackTrace();//Captura la exception
		}
             return peliculas;//Retorna Peliculas
       }
      
 /**Metodo donde se lee el archivo PeliculasDescargada.txt
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
      public static void Readdescargado(){
            try {//Inicio de Try Catch
			BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/PeliculasDescargada.txt"));//Se crea un objeto BufferedReader donde ira el Archivo de .txt a leer
			String line = "";//Se inicializa para buscar en el archivo
			while((line = bufferedReader.readLine())!=null){//Se busca en el archivo
				if(line.indexOf("Pelicula Descargada: ")!= -1){//Si se consigue el archivo se imprime
					System.out.println(line);//Se imprime la linea
				}
			}
		} catch (FileNotFoundException e) {e.printStackTrace();//Captura la exception
		} catch (IOException e) {e.printStackTrace();//Captura la exception
		}
       }    

      
 /**Metodo donde arracanca el servidor de descarga
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */   
    public void runServer(){
      ServerSocket serv=null;//para comunicacion

      Socket sock=null;//Socket Para comunicarse

      boolean listening=true;//Variable para el while true
      try{//Inicio de Try Catch
         serv=new ServerSocket(8082);//Se le asigna el servidor al Server Socket 
         mostrar(".::Servidor activo :");//Mostrar el Estado del servidor de descarga en consola
         Comando comand=new Comando();
         comand.start();
         while(listening){//While cuya condicion es True y comienza el demonio
           
            try {//Inicio del Try Catch
                
               mostrar("Esperando Usuarios");//Mostrar Esperando Usuarios en consola
               sock=serv.accept();//Se acepta la peticion
             
            } catch (IOException e){//Se captura la exception
               mostrar("Aceptado fallado: " + serv + ", " + e.getMessage());//Se imprime Aceptado Fallado
               continue;//continua el while
            }
          
            Servs user=new Servs(sock,this);//Se crea el hilo             
	    user.start();//Se inicia el hilo
            
          
         }
         
      }catch(IOException e){//Se captura la exception
         mostrar("error :"+e);//Mostrar el error
      }
   } 
/**Metodo donde arracanca el servidor de descarga
 * @param args 
 * @since ServidorDescarga 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */  
    public static void main(String[] args) {
        // TODO code application logic here
         ServidorDescarga ser= new ServidorDescarga();//Se inicia el servidor de Descarga
         ser.runServer();//Se comienza el servidor de descarga
    }
    
}
