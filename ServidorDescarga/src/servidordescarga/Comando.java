/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidordescarga;

import java.util.Scanner;
import static servidordescarga.ServidorDescarga.Readdescargado;
import static servidordescarga.ServidorDescarga.Readtxt;

/**Clase que maneja los hilos de los comandos
 *@since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */

public class Comando extends Thread  {
  /**Metodo para los comandos que se introduzca en el servidor central
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */   
    public void run(){
          while (true){
       System.out.println ("Por favor introduzca un comando o pulse enter si no desea escribir comando:");//Se imprime lo que desea el usuario
        String entradaTeclado = "";//Se inicializa lo que introduce el cliente
        Scanner entradaEscaner = new Scanner (System.in); //Creación de un objeto Scanner
        entradaTeclado = entradaEscaner.nextLine (); //Invocamos un método sobre un objeto Scanner
        
            
        
        
        if (entradaTeclado.equals("Peliculas Descargando")==true){//Si el cliente Introduce Peliculas Descargando se lee los texto
            Readtxt();//Se invoca el metodo Readtxt
        }
         if (entradaTeclado.equals("Peliculas Descargadas")==true){//Si el cliente Introduce Peliculas Descargadas se lee los texto
             Readdescargado();//Se invoca el metodo ReadDescargado
            
      
        }
         ServidorDescarga.mostrar("Esperando Usuarios o Introduzca un Comando"); //Se llama el metodo Mostrar
}
    }

}
