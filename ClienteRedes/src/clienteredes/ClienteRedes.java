/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteredes;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**Clase donde se conecta el cliente con el servidor central y el servidor de descarga
 *@since ClienteRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
public class ClienteRedes {

    /**Metodo main para inicializar el programa
     * @param args the command line arguments
     * @since ClienteRedes 1.0
     * @author Jose Suarez
     * @author Mario Salazar
     */

            
    public static void main(String[] args) {
        // TODO code application logic here
        Cliente nuevo=new Cliente();//Se crea un nuevo cliente
        System.out.println ("Cliente");//Imprime Cliente en la consola
        System.out.println ("Por favor introduzca un comando:");//Imprime Por favor introduzca un comando: en la consola
        String entradaTeclado = "";//Se crea vacia para luego llenarlo
        Scanner entradaEscaner = new Scanner (System.in); //Creación de un objeto Scanner
        entradaTeclado = entradaEscaner.nextLine (); //Invocamos un método sobre un objeto Scanner
        if (entradaTeclado.equals("Inscripcion")){//Preguntamos si el cliente se va a Inscribir
            nuevo.Conectar();//Conectamos el cliente
        }
        else//Sino desea se cierra el programa
            System.exit(0);//Se cierra el programa
        
       
       
    }
    
}
