/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteredes;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**Clase Cliente donde se maneja tanto la conexion con el servidor central y descargar la pelicula del servidor de descarga
 * @author Jose Suarez
 * @author Mario Salazar
 * @version 1.0
 * @since ClienteRedes 1.0
 */
public class Cliente {
    
    String ip="127.0.0.1";//Ip del Servidor Central a conectar
     ServerSocket serv=null;//Socket para comunicacion con el servidor de descarga
    DataInputStream  in=null;//Se crea un Data Input para recibir valores del servidor
    DataOutputStream  out=null;//Se crea un Data Outpur para mandar valores al servidor
   String valor="No";//Variable para saber si se descarga con exito la pelicula o si el servidor de descarga esta caido
   
   /**Metodo donde se conecta el cliente con el servidor de descarga el cual returna un string si se logro con exito la descarga o no
    * @return El resultado si se descargo el archivo con exito o no
    * @param addr La direccion ip del servidor de descarga
    * @param Port El puerto de escucha del servidor de descarga
    * @since ClienteRedes 1.0
    */
    public String  ConectarSDescarga (String addr,Integer Port){
        try{
         Socket sk= null; //Se crea el socket
                String mensaje="";//Se crea la variable mensaje donde se alojara lo que mande el servidor
                sk = new Socket(addr,Port); //Se crea el socket con la direccion ip y el puerto del servidor de descarga
                DataOutputStream out = new DataOutputStream(sk.getOutputStream());////Se declara un objeto de tipo DataOutputStream para mandar valores al servidor
                DataInputStream in =new DataInputStream(sk.getInputStream());  //Se declara un objeto de tipo DataOutputStream para mandar obtener al servidor
                mensaje=in.readUTF();//Se lee el mensaje que manda el servidor de descarga
                String mensaje1=in.readUTF();//Se lee el mensaje que manda el servidor de descarga
                String mensaje2=in.readUTF();//Se lee el mensaje que manda el servidor de descarga
                String mensaje3=in.readUTF();//Se lee el mensaje que manda el servidor de descarga
                String mensaje4=in.readUTF();//Se lee el mensaje que manda el servidor de descarga
                String mensaje5=in.readUTF();//Se lee el mensaje que manda el servidor de descarga
                System.out.println(mensaje);//Se imprime el mensaje en la consola
                System.out.println(mensaje1);//Se imprime el mensaje en la consola
                System.out.println(mensaje2);//Se imprime el mensaje en la consola
                System.out.println(mensaje3);//Se imprime el mensaje en la consola
                System.out.println(mensaje4);//Se imprime el mensaje en la consola
                System.out.println(mensaje5);//Se/ imprime el mensaje en la consola
                
                 Scanner entradaEscaner3 = new Scanner (System.in);//Se crea un objeto Scanner para leer lo que el cliente introduce en la consola
                    System.out.println ("Escriba un comando para el servidor:");//Se imprime en la consola para guiar al cliente
                    String entradaTeclado2 = entradaEscaner3.nextLine (); //Invocamos un método sobre un objeto Scanner
                        if (entradaTeclado2.equals("Salir")){//Se crea una sentencia if para comparar si lo que introduce el cliente es igual o no a Salir
                           return valor;//Este retorna valor si lo que coloco el cliente es igual a Salir
                       } 
                        else {//Si no es igual a salir entonces se procede a descargar el archivo  
                       out.writeUTF(entradaTeclado2);//Se manda al servidor la pelicula que pidio el cliente
                       serv=new ServerSocket(8089);//Se crea un ServerSocket para la recibir lo que viene del servidor de descarga
                       Socket w=serv.accept(); //Se acepta al servidor de descarga
                       in=new DataInputStream(w.getInputStream());///Se declara un objeto de tipo DataInputStream para recibir valores del servidor
                       out=new DataOutputStream(w.getOutputStream());///Se declara un objeto de tipo DataOutputStream para mandar valores al servidor
                       String tam=in.readUTF();//Se guarda lo que mando el servidor de descarga el cual es el tamaño del archivo
                       int tamfinal=Integer.parseInt(tam)/1000;//Se convierte el tamaño del archivo a Integer y este se divide entre 1000 ya que el tamaño viene en bytes
                       String nombrearchivo=in.readUTF();//Se guarda lo que mando el servidor de descarga el cual es el nombre del archivo
                       String name="./Src/"+nombrearchivo;//Se coloca la ruta donde se va a guardar el archivo
                       InputStream llegada = w.getInputStream();//Se declara un objeto de tipo InputStream para recibir valores del servidor
                        FileOutputStream destino=new FileOutputStream(name);//Se declara un objeto FileOutputStream el cual creara el archivo en el destino que se especifique en la variable name
                        byte[] buffer = new byte[1024];//Se crea un buffer el cual sera de 1024 byte
                        int len;//Se crea una variable para saber lo que se va leyendo con el buffer del servidor de descarga
                        int sum=0,subtotal=0;//Se crean variables para llevar la cuenta de lo que se lleva descargado del archivo
                        while((len=llegada.read(buffer))>0){ //Se crea un ciclio para la lectura del archivo con el buffer de 1024
               
                          if(w.isInputShutdown()==false){//Se pregunta si el Input esta apagado para notar si el servidor de descarga sigue activo
                          destino.write(buffer,0,len);//Se va copiando en el destino el archivo
                         
                          
                          sum=sum+len;//Se suma lo que lleva el archivo descargado
                          subtotal=(sum/tamfinal)/10;//Sacamos el porcentaje que lleva el archivo descargado 
                          System.out.println("Porcentaje Descargado: "+subtotal+"%");//Se imprime el porcentaje que lleva el archivo
                          destino.flush();////Se va copiando en el destino el archivo
      
                           valor="Si";//Se coloca el valor Si ya que el servidor de descarga sigue activo
                          }
                         
                          } 
                       if (subtotal!=100){//Se pregunta si el total es igual a 100 ya que si no es significa que el archivo no se descargo completamente
                           valor="No";//Se coloca No ya que no se descago completamente el archivo
                       }
                        destino.close();//Se cierra el objeto FIleOutput
                        w.close();//Se cierra el socket
                        sk.close();//Se cierra el socket
                        serv.close();//Se cierra el socket
                        return valor;//Se retorna el valor 
                        }
                      
                      
                      
                      
         
                
            } catch (Exception e) {//Se captura la exception que arroje el try
                System.out.println("Servidor de Descarga caido");//Se imprime en consola Servidor de Descarga caido si este no esta conectado
                 return valor;//Se retorna el valor 
            }
   
    }
    
    /**Metodo para conectar el cliente con el servidor central
     * @since ClienteRedes 1.0
     */
    
    public void Conectar(){
    try {
                Socket sk= null; //Se crea el socket
                Socket sk2=null;//Se crea el socket
                String mensaje="";//Se declara una variable de tipo string
                InetAddress addr = InetAddress.getByName(ip); //Se crea un objeto de tipo InetAddress
               
                      
               sk = new Socket(addr,8081); //Se crea el Socket con la ip y puerto de escucha del servidor
               DataOutputStream out = new DataOutputStream(sk.getOutputStream());////Se declara un objeto de tipo DataOutputStream para mandar valores al servidor
                DataInputStream in =new DataInputStream(sk.getInputStream());  //Se declara un objeto de tipo DataOutputStream para mandar obtener al servidor
                
                    mensaje=in.readUTF();//Se lee el mensaje que manda el servidor central
                    String mensaje1=in.readUTF();//Se lee el mensaje que manda el servidor central
                    System.out.println(mensaje);//Se/ imprime el mensaje en la consola
                    System.out.println(mensaje1);//Se/ imprime el mensaje en la consola
                //Se comienza un do while hasta que el cliente decida irse
                do {
                    
        	       Scanner entradaEscaner3 = new Scanner (System.in);
                    //Mientras el mensaje no sea fin seguira leyendo
                    System.out.println ("Escriba un comando:");//Se imprime en la consola para guiar al cliente
                    String entradaTeclado2 = entradaEscaner3.nextLine (); //Invocamos un método sobre un objeto Scanner
                    out.writeUTF(entradaTeclado2);//Se manda lo que introduce el cliente por consola
                      if (entradaTeclado2.equals("Pelicula")){//Si el cliente quiere una pelicula 
                        mensaje =""; //Se inicializa la variable de string mensaje a vacío
                        mensaje = in.readUTF();////Se lee el mensaje que manda el servidor central
                         if (mensaje.equals("No servidores Disponibles")){//El servidor central notifica si hay Servidores Disponibles
                             System.out.println (mensaje);//Se imprime el mensaje en la consola
                             out.writeUTF("Seguir");//Se manda al servidor central seguir
                             
                         }
                         else{//Si hay servidores disponibles se procede a descargar la pelicula
                          String[] prueba=mensaje.split(",");//Se le aplica un split a lo que recibimos del servidor central
                        
                            if (ConectarSDescarga(prueba[0],Integer.parseInt(prueba[1]))=="Si"){//Si el metodo arroja si se le notifica el servidor que la descarga fue exitosa
                         
                                 out.writeUTF(prueba[2]);//Se le notifica al servidor que servidor de descarga esta libre para ser usado
                                 mensaje =""; //Se inicializa la variable de string mensaje a vacío
                                 mensaje = in.readUTF();////Se lee el mensaje que manda el servidor central
                                 System.out.println(mensaje);//Se imprime el mensaje en la consola
                                 
                            }
                            else { out.writeUTF("No "+prueba[2]);//Se le notifica al servidor que servidor de descarga esta libre para ser usado
                                 mensaje =""; //Se inicializa la variable de string mensaje a vacío
                                 mensaje = in.readUTF();////Se lee el mensaje que manda el servidor central
                                 System.out.println(mensaje);//Se imprime el mensaje en la consola
                            }
                         
                         }
                          
                         
                         
                      }
                      if (entradaTeclado2.equals("Salir")){//Si el cliente quiere salir se le notifica al servidor
                          mensaje =""; //Se inicializa la variable de string mensaje a vacío
                          mensaje = in.readUTF();////Se lee el mensaje que manda el servidor central
                      }
       
                      
         }while (!mensaje.startsWith("FIN")); //La condicion del do while 
            } catch (Exception e) {//Se captura la excepcion si el Servidor Central esta conectado o no
                System.err.println("Se a caido el servidor Central...Reconectado");//Si no esta se notifica al cliente
                Cliente reset=new Cliente();//Se reinicia la conexcion hasta que este disponible el servidor
                reset.Conectar();//Se conecta de nuevo hasta que activo
            }
}
}
