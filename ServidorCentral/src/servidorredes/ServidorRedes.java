/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;

/**Clase donde se inicializa el servidor central
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
public class ServidorRedes {    
    String mensaje="";//Se iniciliza el mensaje
    String ip="127.0.0.1";//Se inicializa el ip del servidor de descarga
    Socket sserv=null;//Se inicializa el Socket 
    
/**Metodo para imprimir el mensaje en la consola del servidor central
 * @param msg el mensaje a imprimir en consola
 * @author Jose Suarez
 * @author Mario Salazar
 */
    public static void mostrar(String msg){
       System.out.println(msg);//Se imprime en la consola del servidor de descarga
   }
    
/**Metodo para modificar la data que entra en el Archivo Numero Descarga
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */  
    public static void Modificartxt(){
        try {//Inicio de segmento Try-Catch
			//Entrada
			BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/NumeroDescarga.txt"));//Se crea un objeto BufferedeReader donde ira el Archivo de .txt a leer
			String line = "";//Se inicializa para buscar si esta la linea a modificar
			while((line = bufferedReader.readLine())!=null){//Se busca si existe la linea
                             
				if(line.indexOf("Esto es linea ")!= -1){//Se pregunta si la linea es la que se busca
                                    line.replaceFirst(line, line);//Se reemplaza la linea
					System.out.println("Encontrado line: "+line);//Se imprime la linea nueva
				}
			}
		} catch (FileNotFoundException e) {e.printStackTrace();//Se captura la exception
		} catch (IOException e) {e.printStackTrace();//Se captura la exception
		}
    }
    
/**Metodo para Escribir la data que entra en el Archivo Localidad Descarga
 * @param localidad donde tiene la localidad desde donde se descarga la pelicula y que pelicula descargo
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */  
    public static void EscribirLocalidad(String localidad){
         try {//Inicio de segmento try catch
			//Escritura
			FileWriter  archivo= new FileWriter("./src/LocalidadDescarga.txt");//Se crea un objeto BufferedeWriter donde ira el Archivo de .txt a leer
			archivo.write("Localidad de Descargado: "+localidad + "\n");//Se forma lo que ira en el archivo en buffer
			archivo.close();
 
		} catch (IOException e) {//Captura la excepction
			// TODO Auto-generated catch block
			e.printStackTrace();//Imprime la exception
		}
    }
    
 /**Metodo para Escribir la data que entra en el Archivo Escribir Numero Descarga
 * @param NumDescarga donde se tiene cuantas veces se han descargado peliculas en el sistema
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */  
    public static void EscribirNumeroDescarga(int NumDescarga){
         try {
			//Escritura
			FileWriter archivo = new FileWriter("./src/NumeroDescarga.txt");//Se crea un objeto BufferedeWriter donde ira el Archivo de .txt a leer
			archivo.write("Numero de Descarga: "+NumDescarga+"\n");//Se forma lo que ira en el archivo en buffer
			archivo.close();
 
		} catch (IOException e) {//Captura la excepction
			// TODO Auto-generated catch block
			e.printStackTrace();//Imprime la exception
		}
    }
    
  /**Metodo para Leer la data que entra en el Archivo Numero de Descarga
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */  
    public static void Readtxt(){
            try {//Inicio de Try Catch
			BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/NumeroDescarga.txt"));//Se crea un objeto BufferedeReader donde ira el Archivo de .txt a leer
			String line = "";//Inicializa para buscar en el archivo
			while((line = bufferedReader.readLine())!=null){//Se busca si existe la linea
				if(line.indexOf("Numero de Descarga: ")!= -1){//Se busca si la linea es la buscada
					System.out.println(line);//Imprime la line
				}
			}
		} catch (FileNotFoundException e) {e.printStackTrace();//Captura la exception
		} catch (IOException e) {e.printStackTrace();//Captura la exception
		}
       }
    
/**Metodo para Leer la data que entra en el Archivo Localidad de Descarga
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */  
    public static void Readdescargado(){
            try {//Inicio de Try Catch
			BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/LocalidadDescarga.txt"));//Se crea un objeto BufferedeReader donde ira el Archivo de .txt a leer
			String line = "";//Inicializa para buscar en el archivo
			while((line = bufferedReader.readLine())!=null){//Se busca si existe la linea
				if(line.indexOf("Localidad de Descargado: ")!= -1){//Se busca si la linea es la buscada
					System.out.println("Desde donde se han descargando las peliculas "+line);//Imprime la line
				}
			}
		} catch (FileNotFoundException e) {e.printStackTrace();//Captura la exception
		} catch (IOException e) {e.printStackTrace();//Captura la exception
		}
       }
    
    
  /**Metodo para Correr el servidor
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */   
    public void runServer(){
      ServerSocket serv=null;//Inicializo ServerSocket para comunicacion
      boolean escucho=true;//Boolean para la condicion del while
      try{//Inicio del Try Catch
         serv=new ServerSocket(8081);//Inicio el Serversocket por el puerto por donde se va a escuchar
         mostrar(".::Servidor activo :");//Se llama el metodo Mostrar
         Comando comand=new Comando();
         comand.start();
         while(escucho){//Inicio del Demonio o While True
            Socket sock=null;//Se crea un Socket para aceptar las conexiones 
            try {//Inicio de Try Catch
           
               mostrar("Esperando Usuarios o Introduzca un Comando"); //Se llama el metodo Mostrar       
               sock=serv.accept();//Se acepta la peticion del cliente que llega 
            } catch (IOException e){//Inicio del Catch
               mostrar("Cliente No aceptado  " + serv + ", " + e.getMessage());//Se llama  el metodo Mostrar
               continue;//Se continua el ciclo
            }
             mostrar("Usario Aceptado"); //Se llama el metodo Mostrar     
            Servs user=new Servs(sock,this);//Se crea el hilo con el cliente      
	    user.start();//Se inicia el cliente
              mostrar("Usuario Aceptado"); //Se llama el metodo Mostrar       
         }
         
      }catch(IOException e){//Se captura la exception
         mostrar("Error al crear el servidor :"+e);//Se llama mostrar para informar del error
      }
   }
    
   /**Metodo Main donde se incializa el servidor
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
         ServidorRedes ser= new ServidorRedes();//Se crea el ServidorRedes 
         ser.runServer();//Se inicia el servidor
    }
    
    }
