/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import static servidorredes.ServidorRedes.Readdescargado;
import static servidorredes.ServidorRedes.Readtxt;
import static servidorredes.Servs.NumDescarga;
import static servidorredes.Servs.servidor1;
import static servidorredes.Servs.servidor2;
import static servidorredes.Servs.servidor3;
import static servidorredes.Servs.servidor4;
import static servidorredes.Servs.servidor5;

/**Clases que maneja los hilos de los comandos
 * @since ServidorRedes 1.0
 * @author Mario Salazar
 * @author Jose Suarezs
 */
public class Comando extends Thread {
    String mensaje="";
    String ip="127.0.0.1";
   /**Metodo para los comandos que se introduzca en el servidor central
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */   

 
    public void run(){
     while (true){
          
        String entradaTeclado = "";//Se inicializa lo que el cliente vaya a introducir
        Scanner entradaEscaner = new Scanner (System.in); //Creación de un objeto Scanner
        entradaTeclado = entradaEscaner.nextLine (); //Invocamos un método sobre un objeto Scanner
        if (entradaTeclado.equals("Peliculas Servidor")==true){//Si el la entrada es igual a Peliculas Servidor se procede a pedir las peliculas del servidor de descarga
            try {//Se inicia el try Catch
                Socket sserv=new Socket(ip,8082);//Se iniciliza el socket para comunicarse con el servidor
                DataInputStream in =new DataInputStream(sserv.getInputStream());  //Se declara un objeto de tipo DataOutputStream para mandar obtener al servidor
                    mensaje=in.readUTF();//Se lee y se guarda lo que nos mande el servidor de descarga
                    String mensaje1=in.readUTF();//Se lee y se guarda lo que nos mande el servidor de descarga
                    String mensaje2=in.readUTF();//Se lee y se guarda lo que nos mande el servidor de descarga
                    String mensaje3=in.readUTF();//Se lee y se guarda lo que nos mande el servidor de descarga
                    String mensaje4=in.readUTF();//Se lee y se guarda lo que nos mande el servidor de descarga
                     String mensaje5=in.readUTF();//Se lee y se guarda lo que nos mande el servidor de descarga
                    System.out.println(mensaje1);//Se imprime el mensaje del servidor de descarga
                    System.out.println(mensaje2);//Se imprime el mensaje del servidor de descarga
                    System.out.println(mensaje3);//Se imprime el mensaje del servidor de descarga
                    System.out.println(mensaje4);//Se imprime el mensaje del servidor de descarga
                    System.out.println(mensaje5);//Se imprime el mensaje del servidor de descarga
                   
                    
                    sserv.close();//Se cierra la conexion con el servidor de descarga
            } catch (IOException ex) {//Se captura la exception
                java.util.logging.Logger.getLogger(ServidorRedes.class.getName()).log(Level.SEVERE, null, ex);//Se imprime la exception
            }
            
        
        }
        
        if (entradaTeclado.equals("Numero Descarga")==true){//Si el comando a introducir es igual a Numero Descarga se lee el NumeroDescarga.txt
            System.out.println("HOLA");//Se imprime el mensaje del servidor de descarga
            Readtxt();//Se invoca el metodo ReadTxt
        }
         if (entradaTeclado.equals("Localidad Descarga")==true){//Si el comando a introducir es igual a Localidad Descarga se lee el LocalidadDescarga.txt
             Readdescargado();//Se invoca el metodo Readdescargado
            
        
       }
          ServidorRedes.mostrar("Esperando Usuarios o Introduzca un Comando"); //Se llama el metodo Mostrar
     }
     }
}
