/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.Vector;

/**Clase Servs donde se inicializa el hilo
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
public class Servs extends Thread {
    Socket scli=null;//Se crea el Socket Cliente
    Socket scli2=null;//Se crea el Socket Cliente
    Socket sserv=null;//Se crea el Socket Cliente
    public static int NumDescarga=0,cantCliente=0,cantCliente1=0,cantCliente2=0,cantCliente3=0,cantCliente4=0,cantCliente5=0;//Se crea cuantas veces se han descargado peliculas exitosas y la cantidad de clientes
    DataInputStream entrada=null, in=null;//Se crea objetos DataInput para ver lo que manda el cliente  
    DataOutputStream salida2=null, out=null;//Se crea objetos DataOutput para ver lo que se le manda al cliente
    public static Vector<Socket> clientes=new Vector();//Se crea un vector de tipo Socket donde se alojan los clientes
     ServidorRedes serv;//Servidor Central
     String ip="127.0.0.1";//Ip del Servidor 1
     String ip2="127.0.0.1";//Ip del Servidor 2
     String ip3="127.0.0.1";//Ip del Servidor 3
     String ip4="127.0.0.1";//Ip del Servidor 4
     String ip5="127.0.0.1";//Ip del Servidor 5
     public static boolean servidor1=false;//Boolean para saber si el Servidor 1 Esta disponible
     public static boolean servidor2=false;//Boolean Para saber si el Servidor 2 Esta disponible
     public static boolean servidor3=false;//Boolean para saber si el Servidor 3 Esta disponible
     public static boolean servidor4=false;//Boolean para saber si el Servidor 4 Esta disponible
     public static boolean servidor5=false;//Boolean para saber si el Servidor 5 Esta disponible
     
/**Constructor Servs donde se crea el cliente en el hilo
 * @param scliente el socket del cliente
 * @param serv  el socket del servidor
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */

     
      public Servs(Socket scliente,ServidorRedes serv) throws IOException {
        scli=scliente;//Se guarda el socket del cliente
        this.serv=serv;//Se instancia el servidor
        clientes.add(scli);//Se agrega el cliente al vector
        serv.mostrar("cliente agregado: "+ scli);//Se muestra que el cliente se agrego al servidor			
     }
 /**Clase Run donde se corre el hilo
 * 
 * @since ServidorRedes 1.0
 * @author Jose Suarez
 * @author Mario Salazar
 */
     public void run(){
    	
    	
    	try{//Inicio de Try Catch
          entrada=new DataInputStream(scli.getInputStream());//Se crea un objeto DataInput para ver que recibe del cliente
          salida2=new DataOutputStream(scli.getOutputStream());//Se crea un objeto DataOutput para ver que manda al cliente  
          salida2.writeUTF(".:: Bienvenido  :");//Se le envia al cliente 
          salida2.writeUTF(".::Esperando Comando:");//Se le envia al cliente
          
          
        
    	}
    	catch (IOException e) {  System.err.println("Error al abrir el socket");     }//Captura la exception
    	
                
    	while(true){//Inicio del demonio o while true
          try{//Inicio de Try Catch
              String mensaje="";//Se inicia el mensaje
              mensaje=entrada.readUTF();//Se lea lo que mande cliente
              System.out.println(mensaje);//Se imprime el mensaje
              if (mensaje.equals("Salir")){//Se pregunta si el cliente quiere salir
                salida2.writeUTF("FIN");//Se le manda al cliente FIN
             }
             
              if (mensaje.equals("Pelicula")){//Se pregunta si el cliente quiere Pelicula
                if ((servidor1 == false )&&(cantCliente!=2)){//Se pregunta si el servidor 1 esta disponible y la cantidad de cliente es distinto de 2
                    servidor1=true;//Se cambia a true de que el servidor ya no esta disponible
                       NumDescarga++;//Se le suma 1 al Numero de Descarga
                  ServidorRedes.Modificartxt();//Se invoca el metodo ModificarTxt de Servidor Redes
                  ServidorRedes.EscribirNumeroDescarga(NumDescarga);//Se invoca el metodo EscribirNumeroDescarga de Servidor Redes
                    salida2.writeUTF(ip+","+8082+",servidor1");//Se le notifica al cliente IP, el puerto y cual servidor tiene asignado
                }
                else if ((servidor2 == false )&&(cantCliente3!=2)){//Se pregunta si el servidor 2 esta disponible y la cantidad de cliente es distinto de 2
                    servidor2=true;//Se cambia a true de que el servidor ya no esta disponible
                       NumDescarga++;//Se le suma 1 al Numero de Descarga
                  ServidorRedes.Modificartxt();//Se invoca el metodo ModificarTxt de Servidor Redes
                  ServidorRedes.EscribirNumeroDescarga(NumDescarga);//Se invoca el metodo EscribirNumeroDescarga de Servidor Redes
                  salida2.writeUTF(ip2+","+8082+",servidor2");//Se le notifica al cliente IP, el puerto y cual servidor tiene asignado
              }
                else if ((servidor3 == false )&&(cantCliente3!=2)){//Se pregunta si el servidor 3 esta disponible y la cantidad de cliente es distinto de 2
                    servidor3=true;//Se cambia a true de que el servidor ya no esta disponible
                       NumDescarga++;//Se le suma 1 al Numero de Descarga
                  ServidorRedes.Modificartxt();//Se invoca el metodo ModificarTxt de Servidor Redes
                  ServidorRedes.EscribirNumeroDescarga(NumDescarga);//Se invoca el metodo EscribirNumeroDescarga de Servidor Redes
                  salida2.writeUTF(ip3+","+8082+",servidor3");//Se le notifica al cliente IP, el puerto y cual servidor tiene asignado
              }
                else if ((servidor4 == false )&&(cantCliente4!=2)){//Se pregunta si el servidor 4 esta disponible y la cantidad de cliente es distinto de 2
                    servidor4=true;//Se cambia a true de que el servidor ya no esta disponible
                       NumDescarga++;//Se le suma 1 al Numero de Descarga
                  ServidorRedes.Modificartxt();//Se invoca el metodo ModificarTxt de Servidor Redes
                  ServidorRedes.EscribirNumeroDescarga(NumDescarga);//Se invoca el metodo EscribirNumeroDescarga de Servidor Redes
                  salida2.writeUTF(ip4+","+8082+",servidor4");//Se le notifica al cliente IP, el puerto y cual servidor tiene asignado
                  
              } else if((servidor5 == false )&&(cantCliente5!=2)){//Se pregunta si el servidor 5 esta disponible y la cantidad de cliente es distinto de 2
                  servidor5=true;//Se cambia a true de que el servidor ya no esta disponible
                     NumDescarga++;//Se le suma 1 al Numero de Descarga
                  ServidorRedes.Modificartxt();//Se invoca el metodo ModificarTxt de Servidor Redes
                  ServidorRedes.EscribirNumeroDescarga(NumDescarga);//Se invoca el metodo EscribirNumeroDescarga de Servidor Redes
                  salida2.writeUTF(ip5+","+8082+",servidor5");//Se le notifica al cliente IP, el puerto y cual servidor tiene asignado
              }
                
              else salida2.writeUTF("No servidores Disponibles");//No existen servidores disponibles
             }
              if(mensaje.equals ("servidor1")){//Si la descarga fue exitosa se libera el servidor 
                  servidor1=false;//Se cambia a true de que el servidor ya esta disponible
                  ServidorRedes.EscribirLocalidad(scli.getLocalAddress().toString());
                  salida2.writeUTF("Descarga Exitosa");//Se le notifica al cliente que la Descarga fue exitosa
              }
              if(mensaje.equals ("servidor2")){//Si la descarga fue exitosa se libera el servidor 
                  servidor2=false;//Se cambia a true de que el servidor ya  esta disponible
                  ServidorRedes.EscribirLocalidad(scli.getLocalAddress().toString());//Se guarda la localidad y la pelicula que se descargo
                  salida2.writeUTF("Descarga Exitosa");//Se le notifica al cliente que la Descarga fue exitosa
              }
              if(mensaje.equals ("servidor3")){//Si la descarga fue exitosa se libera el servidor 
                  servidor3=false;//Se cambia a true de que el servidor ya esta disponible
                  ServidorRedes.EscribirLocalidad(scli.getLocalAddress().toString());//Se guarda la localidad y la pelicula que se descargo
                  salida2.writeUTF("Descarga Exitosa");//Se le notifica al cliente que la Descarga fue exitosa
              }
              if(mensaje.equals ("servidor4")){//Si la descarga fue exitosa se libera el servidor 
                  servidor4=false;//Se cambia a true de que el servidor ya  esta disponible
                  ServidorRedes.EscribirLocalidad(scli.getLocalAddress().toString());//Se guarda la localidad y la pelicula que se descargo
                  salida2.writeUTF("Descarga Exitosa");//Se le notifica al cliente que la Descarga fue exitosa
              }
              if(mensaje.equals ("servidor5")){//Si la descarga fue exitosa se libera el servidor 
                  servidor5=false;//Se cambia a true de que el servidor ya  esta disponible
                  ServidorRedes.EscribirLocalidad(scli.getLocalAddress().toString());//Se guarda la localidad y la pelicula que se descargo
                  salida2.writeUTF("Descarga Exitosa");//Se le notifica al cliente que la Descarga fue exitosa
              }
              if(mensaje.equals ("No servidor1")){//Si la descarga no fue exitosa se libera el servidor 
                  servidor1=false;//Se cambia a true de que el servidor ya  esta disponible
                  salida2.writeUTF("Descarga No Exitosa");//Se le notifica al cliente que la Descarga no fue exitosa
              }
              if(mensaje.equals ("No servidor2")){//Si la descarga no fue exitosa se libera el servidor 
                  servidor2=false;//Se cambia a true de que el servidor ya  esta disponible     
                  salida2.writeUTF("Descarga No Exitosa");//Se le notifica al cliente que la Descarga no fue exitosa
              }
              if(mensaje.equals ("No servidor3")){//Si la descarga no fue exitosa se libera el servidor 
                  servidor3=false;//Se cambia a true de que el servidor ya  esta disponible
                  salida2.writeUTF("Descarga No Exitosa");//Se le notifica al cliente que la Descarga no fue exitosa
              }
              if(mensaje.equals ("No servidor4")){//Si la descarga no fue exitosa se libera el servidor 
                  servidor4=false;//Se cambia a true de que el servidor ya  esta disponible
                  salida2.writeUTF("Descarga No Exitosa");//Se le notifica al cliente que la Descarga no fue exitosa
              }
              if(mensaje.equals ("No servidor5")){//Si la descarga no fue exitosa se libera el servidor 
                servidor5=false;//Se cambia a true de que el servidor ya  esta disponible
                  salida2.writeUTF("Descarga No Exitosa");//Se le notifica al cliente que la Descarga no fue exitosa
              }
              if (mensaje.equals("Seguir")){//Si la descarga fue exitosa sigue el Programa activo
                  salida2.writeUTF("");//Se manda vacio al cliente
              }
               
  
             mensaje="";//Se resetea el mensaje de forma de no guardar basura
          }
          catch (IOException e) {System.out.println("El cliente termino la conexion");break;}//Se captura la exception si el cliente se desconecto
    	}
    	serv.mostrar("Se removio un usuario");//Se muestra que el cliente se removio
    	
    	try{//Inicio de Try Catch
          serv.mostrar("Se desconecto un usuario");//Se muestra que el cliente se desconecto
          scli.close();//Se cierra el cliente
    	}	
        catch(Exception et)
        {serv.mostrar("no se puede cerrar el socket");}//Se captura la exception y se muestra que no se puede cerrar el socket   
     }
     
    
}
